* input file for EMC3 code
*** 1. fein grid mesh representing magnetic coordinates
1
10000   10000   10   
*** 2. Non default, Non Transparant, Plate surface
*** 2.1 Non default
* radial
0
* poloidal
2
0  0  1
0  9998  0  8
9999  0  1
0  9998  0  8
* toroidal
2
0  0  3
0  9998  0  9998
9  0  3
0  9998  0  9998
*** 2.2 Non transparant
* radial
2
3  0  1
0  9998  0  8
9998  0  -1
0  9998  0  8
* poloidal
0
* toroidal
0
*** 2.3 Plate surface
* radial
0
* poloidal
0
* toroidal
2
8  0  1  
9997  9998  0  9998
8  0  -1  
9997  9998  0  9998
*** 3. Physical cell
10  1
* check cell ?
F