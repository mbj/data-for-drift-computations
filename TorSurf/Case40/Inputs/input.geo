* input file for EMC3 code
*** 1. fein grid mesh representing magnetic coordinates
1
200   200   40   
*** 2. Non default, Non Transparant, Plate surface
*** 2.1 Non default
* radial
0
* poloidal
2
0  0  1
0  198  0  38
199  0  1
0  198  0  38
* toroidal
2
0  0  3
0  198  0  198
39  0  3
0  198  0  198
*** 2.2 Non transparant
* radial
2
3  0  1
0  198  0  38
198  0  -1
0  198  0  38
* poloidal
0
* toroidal
0
*** 2.3 Plate surface
* radial
0
* poloidal
0
* toroidal
2
38  0  1  
197  198  0  198
38  0  -1  
197  198  0  198
*** 3. Physical cell
10  1
* check cell ?
F