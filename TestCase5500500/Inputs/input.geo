* input file for EMC3 code
*** 1. fein grid mesh representing magnetic coordinates
1
500   500   5   
*** 2. Non default, Non Transparant, Plate surface
*** 2.1 Non default
* radial
0
* poloidal
2
0  0  1
0  498  0  3
499  0  1
0  498  0  3
* toroidal
2
0  0  3
0  498  0  498
4  0  3
0  498  0  498
*** 2.2 Non transparant
* radial
2
3  0  1
0  498  0  3
498  0  -1
0  498  0  3
* poloidal
0
* toroidal
0
*** 2.3 Plate surface
* radial
0
* poloidal
0
* toroidal
2
3  0  1  
497  498  0  498
3  0  -1  
497  498  0  498
*** 3. Physical cell
10  1
* check cell ?
F