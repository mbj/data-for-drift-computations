* input file for EMC3 code
*** 1. fein grid mesh representing magnetic coordinates
1
50   20   10   
*** 2. Non default, Non Transparant, Plate surface
*** 2.1 Non default
* radial
0
* poloidal
2
0  0  1
0  48  0  8
19  0  1
0  48  0  8
* toroidal
2
0  0  3
0  48  0  18
9  0  3
0  48  0  18
*** 2.2 Non transparant
* radial
2
3  0  1
0  18  0  8
48  0  -1
0  18  0  8
* poloidal
0
* toroidal
0
*** 2.3 Plate surface
* radial
0
* poloidal
0
* toroidal
2
8  0  1  
47  48  0  18
8  0  -1  
47  48  0  18
*** 3. Physical cell
10  1
* check cell ?
F