* input file for EMC3 code
*** 1. fein grid mesh representing magnetic coordinates
1
100   100   10   
*** 2. Non default, Non Transparant, Plate surface
*** 2.1 Non default
* radial
0
* poloidal
2
0  0  1
0  98  0  8
99  0  1
0  98  0  8
* toroidal
2
0  0  3
0  98  0  98
9  0  3
0  98  0  98
*** 2.2 Non transparant
* radial
2
3  0  1
0  98  0  8
98  0  -1
0  98  0  8
* poloidal
0
* toroidal
0
*** 2.3 Plate surface
* radial
0
* poloidal
0
* toroidal
2
8  0  1  
97  98  0  98
8  0  -1  
97  98  0  98
*** 3. Physical cell
10  1
* check cell ?
F