import numpy as np
import math
import os

def generate_vertices(nb_tor,nb_rad,nb_pol,toroidal,):

    arr = np.array([1, 2, 3, 4, 5])

    toroidal



    # Write the required geometry information to a text file
    gd = open("GEOMETRY_3D_DATA","w+")
    gd.write("teststring")

    return


def create_toroidal_vertices(nb_tor,nb_rad,nb_pol,toroidal_range, R, Rmin):
    # nb_tor, nb_rad, nb_pol are the number of surfaces in each direction
    # R (major radius) and a (minor radius) give the dimension of the torus
    # Create array containing angles of toroidal surfaces
    toroidal_surfaces = np.linspace(0, toroidal_range, nb_tor)  # Include first and last surface
    # Initialize arrays for the radial and horizontal positions
    # Radial position R = R0 (major radius) + a (minor radius)*cos(phi)
    radial_positions = np.empty(shape=(nb_tor,nb_rad*nb_pol),dtype='float')
    horizontal_positions = np.empty(shape=(nb_tor,nb_rad*nb_pol),dtype='float')
    r = np.linspace(Rmin/2,Rmin,nb_rad)
    for i in range(nb_tor):
        for j in range(nb_pol):
            for k in range(nb_rad):
                radial_positions[i,j*nb_rad+k] = R+r[k]*math.cos(j*2*math.pi/nb_pol)
                horizontal_positions[i,j*nb_rad+k] = r[k]*math.sin(j*2*math.pi/nb_pol)
    return [toroidal_surfaces,radial_positions,horizontal_positions]


def writeGEOMETRY3DDATA(tor_surf,rad_pos,hor_pos, ntheta, nr, nz, path):
    # Open file to write the geometry information
    filepath = path+"\\GEOMETRY_3D_DATA"
    gd = open(filepath, "w+")
    gd.write(str(nr)+"   ")
    gd.write(str(nz)+"   ")
    gd.write(str(ntheta)+"   ")
    gd.write("\n")
    for i in range(len(tor_surf)):
        gd.write(str(tor_surf[i]))
        gd.write('\n')
        for j in range(math.ceil(len(rad_pos[i])/6)):
            for k in range(min(len(rad_pos[i,j*6:]),6)):
                fs = "{:.6f}".format(rad_pos[i,j*6+k])
                gd.write(str(fs+'  '))
            gd.write('\n')
        for j in range(math.ceil(len(hor_pos[i])/6)):
            for k in range(min(len(hor_pos[i,j*6:]),6)):
                fs = "{:.6f}".format(hor_pos[i,j*6+k])
                gd.write(str(fs+'  '))
            gd.write('\n')

    return


def imposeAnalytic(tor_surf,rad_pos,hor_pos,analytic_description,Rmajor, Rminor, path):
    bfs = open(path, "w+")
    for i in range(len(tor_surf)):
        for j in range(math.ceil(len(rad_pos[i])/10)):
            for k in range(min(len(rad_pos[i,j*10:]),10)):
                value = analytic_description(rad_pos[i,j*10+k],hor_pos[i,j*10+k],tor_surf[i],Rmajor,Rminor)
                fs = "{:.6f}".format(value)
                bfs.write(str(fs+'  '))
            bfs.write('\n')
    return

def b_field_description(r,z,theta):
    # Radial B-field (linear and max at central surfaces of the torus
    R = math.sqrt((r-500)**2 + z**2)+0.01+0*theta
    bval = 0.01/R*100  # R in cm
    return bval

def Bfieldvector(r,z,theta):
    R = math.sqrt((r - 500) ** 2 + z ** 2) + 0.01
    Br = 0
    Bz = math.cos(theta/180*1/10*math.pi)*0.01
    Btheta = 0.01/R*100
    return [Br,Bz,Btheta]

def CreateGeneralInfo(filepath, nz,r,z,theta):
    # nz is the number of zones considered
    # r,z,theta are the number of surfaces in all directions
    # They should be lists with length nz
    gi = open(filepath,"w+")
    gi.write("* input file for EMC3 code")
    gi.write('\n')
    gi.write("*** 1. fein grid mesh representing magnetic coordinates")
    gi.write('\n')
    for i in range(nz):
        gi.write(str(i+1))
        gi.write("\n")
        gi.write(str(r[i])+'   ')
        gi.write(str(z[i])+'   ')
        gi.write(str(theta[i])+'   ')
        gi.write("\n")
        gi.write("*** 2. Non default, Non Transparant, Plate surface")
        gi.write("\n")
        gi.write("*** 2.1 Non default")
        gi.write("\n")
        gi.write("* radial")
        gi.write("\n")
        gi.write("0")
        gi.write("\n")
        gi.write("* poloidal")
        gi.write("\n")
        gi.write("2")
        gi.write("\n")
        gi.write("0  "+str(i)+"  1")
        gi.write("\n")
        gi.write("0  "+str(r[i]-2)+"  0  "+str(theta[i]-2))
        gi.write("\n")
        gi.write(str(z[i]-1)+"  "+str(i)+"  1")
        gi.write("\n")
        gi.write("0  "+str(r[i]-2)+"  0  "+str(theta[i]-2))
        gi.write("\n")
        gi.write("* toroidal")
        gi.write("\n")
        gi.write("2")
        gi.write("\n")
        gi.write("0  "+str(i)+"  3")
        gi.write("\n")
        gi.write("0  "+str(r[i]-2)+"  0  "+str(z[i]-2))
        gi.write("\n")
        gi.write(str(theta[i]-1)+"  "+str(i)+"  3")
        gi.write("\n")
        gi.write("0  "+str(r[i]-2)+"  0  "+str(z[i]-2))
        gi.write("\n")
        gi.write("*** 2.2 Non transparant")
        gi.write("\n")
        gi.write("* radial")
        gi.write("\n")
        gi.write("2")
        gi.write("\n")
        gi.write("3  "+str(i)+"  1")
        gi.write("\n")
        gi.write("0  "+str(z[i]-2)+"  0  "+str(theta[i]-2))
        gi.write("\n")
        gi.write(str(r[i]-2)+"  "+str(i)+"  -1")
        gi.write("\n")
        gi.write("0  "+str(z[i]-2)+"  0  "+str(theta[i]-2))
        gi.write("\n")
        gi.write("* poloidal")
        gi.write("\n")
        gi.write("0")
        gi.write("\n")
        gi.write("* toroidal")
        gi.write("\n")
        gi.write("0")
        gi.write("\n")
        gi.write("*** 2.3 Plate surface")
        gi.write("\n")
        gi.write("* radial")
        gi.write("\n")
        gi.write("0")
        gi.write("\n")
        gi.write("* poloidal")
        gi.write("\n")
        gi.write("0")
        gi.write("\n")
        gi.write("* toroidal")
        gi.write("\n")
        gi.write("2")
        gi.write("\n")
        gi.write(str(theta[i]-2)+"  "+str(i)+ "  1  ")
        gi.write("\n")
        gi.write(str(r[i]-3)+"  "+str(r[i]-2)+"  0  "+str(z[i]-2) )
        gi.write("\n")
        gi.write(str(theta[i]-2)+"  "+str(i)+ "  -1  ")
        gi.write("\n")
        gi.write(str(r[i]-3)+"  "+str(r[i]-2)+"  0  "+str(z[i]-2))
        gi.write("\n")
    gi.write("*** 3. Physical cell")
    gi.write("\n")
    gi.write("10  1") # Geometric cell = Physical cell
    gi.write("\n")
    gi.write("* check cell ?")
    gi.write("\n")
    gi.write("F")
    return None







