import numpy as nump
import matplotlib.pyplot as plt
import math
azimuths = nump.radians(nump.linspace(0, 360, 20))
zeniths = nump.arange(0, 100, 10)


def b_field_description(r,z,theta, Rmajor, Rminor):
    # Radial B-field (linear and max at central surfaces of the torus
    R = math.sqrt((r-Rmajor)**2 + z**2)+0.01+0*theta
    R = 0.01*R
    bval = math.exp(-R) # R in cm
    return bval

r, theta = nump.meshgrid(zeniths, azimuths)
Bval = nump.empty((len(r),len(r[0])))

for i in range(len(r)):
    for j in range(len(r[0])):
        Bval[i][j] =b_field_description(r[i][j],0,theta[i][j],100,100)
values = nump.random.random((azimuths.size, zeniths.size))
print(r)
print(theta)
#-- Plot... ------------------------------------------------
fig, ax = plt.subplots(subplot_kw=dict(projection='polar'))
ax.contourf(theta, r, Bval)
ax.colorbar()
plt.show()
