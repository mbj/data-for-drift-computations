
import math
import matplotlib
import matplotlib.pyplot as plt
import numpy as nump

def plotfluxTube(tor,rad,hor,nt,nr,np,field_lines,show=True):
    plt.figure()
    ax = plt.axes(projection='3d')
    # Lines following toroidal direction

    xlines_tor = [None] * np * nr
    ylines_tor = [None] * np * nr
    zlines_tor = [None] * np * nr
    for i in range(0,2):
        xtemp = []
        ytemp = []
        ztemp = []
        xtemp2 = []
        ytemp2= []
        ztemp2 = []
        for k in range(nt):
            xtemp.append(rad[k, i] * math.cos(tor[k] * math.pi / 180))
            ytemp.append(rad[k, i] * math.sin(tor[k] * math.pi / 180))
            ztemp.append(hor[k, i])
            xtemp2.append(rad[k, i+nr] * math.cos(tor[k] * math.pi / 180))
            ytemp2.append(rad[k, i+nr] * math.sin(tor[k] * math.pi / 180))
            ztemp2.append(hor[k, i+nr])
        xlines_tor[i] = xtemp
        ylines_tor[i] = ytemp
        zlines_tor[i] = ztemp
        xlines_tor[i+2] = xtemp2
        ylines_tor[i+2] = ytemp2
        zlines_tor[i+2] = ztemp2

    if field_lines == None:
        for i in range(4):
            ax.plot(xlines_tor[i], ylines_tor[i], zlines_tor[i], color='green')
    else:
        for i in range(len(field_lines)):
            ax.plot(field_lines[i][0], field_lines[i][1], field_lines[i][2],color='green')

    # Poloidal lines

    xlines_pol = [None] * nt * nr
    ylines_pol = [None] * nt * nr
    zlines_pol = [None] * nt * nr
    for i in range(nt):
        for j in range(2):
            xtemp = []
            ytemp = []
            ztemp = []
            for k in range(2):
                xtemp.append(rad[i, k * nr + j] * math.cos(tor[i] * math.pi / 180))
                ytemp.append(rad[i, k * nr + j] * math.sin(tor[i] * math.pi / 180))
                ztemp.append(hor[i, k * nr + j])
            xtemp.append(xtemp[0])
            ytemp.append(ytemp[0])
            ztemp.append(ztemp[0])
            xlines_pol[i * nr + j] = xtemp
            ylines_pol[i *nr + j] = ytemp
            zlines_pol[i* nr + j] = ztemp
            ax.plot(xlines_pol[i * nr + j], ylines_pol[i  * nr + j], zlines_pol[i  * nr + j],
                    color='green')

    # Radial lines

    xlines_rad = [None] * np * nt
    ylines_rad = [None] * np * nt
    zlines_rad = [None] * np * nt
    for i in range(nt):
        for j in range(2):
            xtemp = []
            ytemp = []
            ztemp = []
            for k in range(2):
                xtemp.append(rad[i, j * nr + k] * math.cos(tor[i] * math.pi / 180))
                ytemp.append(rad[i, j * nr + k] * math.sin(tor[i] * math.pi / 180))
                ztemp.append(hor[i, j * nr + k])
            xtemp.append(xtemp[0])
            ytemp.append(ytemp[0])
            ztemp.append(ztemp[0])
            xlines_rad[i  * nr + j] = xtemp
            ylines_rad[i  * nr + j] = ytemp
            zlines_rad[i  * nr + j] = ztemp
            ax.plot(xlines_rad[i * nr + j], ylines_rad[i * nr + j], zlines_rad[i * nr + j],
                    color='green')

    ax.set_xlabel('X [cm]')
    ax.set_ylabel('Y [cm]')
    ax.set_zlabel('Z [cm]')
    ax.set_title('Single flux tube for helical magnetic field')
    if show:
        plt.show()

    return [xlines_pol,xlines_rad,xlines_tor,ylines_pol,ylines_rad,ylines_tor,zlines_pol,zlines_rad,zlines_tor]
