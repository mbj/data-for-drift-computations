# Plot the vertices generated for the creation of a mesh in EMC3
from mpl_toolkits import mplot3d
import numpy as nump
import matplotlib
import matplotlib.pyplot as plt
from MeshFunctions import *
import math
from PlottingFunctions import *

# General Info about the case to plot
nt =1; nr = 10; np = 100
Rmajor = 500
rminor = 100
Trange = 36
# Create the vertices
[tor,rad,hor] = create_toroidal_vertices(nt,nr,np,Trange,Rmajor,rminor)
# PLot the grid points
plt.figure(1)
[xdata,ydata,zdata] = plotGridPoints(tor,rad,hor,show = True)

# Plot lines connecting the vertices in all different directions, this will be used to plot the mesh
plt.figure(2)
[xlines_pol,xlines_rad,xlines_tor,ylines_pol,ylines_rad,ylines_tor,zlines_pol,zlines_rad,zlines_tor] = plotMesh(tor,rad,hor,np,nr,nt,show=True)
#plt.show()
# Make a 2D plot
for i in range(len(xlines_tor)):
    plt.plot(xlines_tor[i],zlines_tor[i],color='green')
for i in range(len(xlines_pol)):
    plt.plot(xlines_pol[i],zlines_pol[i],color='green')
for i in range(len(xlines_rad)):
    plt.plot(xlines_rad[i],zlines_rad[i],color='green')
plt.xlabel("X [cm]")
plt.ylabel("Z [cm]")
plt.title(" Toroidal section of the mesh")
plt.show()
# Plot the magnetic and potential values

def b_field_description(r,z,theta, Rmajor, Rminor):
    # Radial B-field (linear and max at central surfaces of the torus
    R = math.sqrt((r-Rmajor)**2 + z**2)+0.01+0*theta
    bval = math.exp(-R/Rmajor) # R in cm
    return bval
Bfield = [0]*nr*nt*np
rdata = [0]*nr*nt*np
for i in range(nr*nt*np):
    Bfield[i] = b_field_description(math.sqrt(xdata[i]**2+ydata[i]**2),zdata[i], math.atan2(ydata[i],xdata[i]),Rmajor,rminor)
    rdata[i] = math.sqrt(xdata[i]**2+ydata[i]**2)

#plt.figure(2)
#ax = plt.axes(projection='3d')
#ax.scatter3D(xdata, ydata, zdata, Bfield, cmap='plasma')
#matplotlib.colorbar.ColorbarBase(ax=ax, values=sorted(Bfield),
 #                                orientation="horizontal")
#plt.show()
print(Bfield)
plt.figure(3)
# Plot 2d contour of Bfield
ns = 100
plot2DContour(ns,ns,Rmajor-rminor,Rmajor+rminor,-rminor,rminor,Rmajor,rminor,b_field_description,'X [cm]', 'Y [cm]','Magnetic field strength',True)