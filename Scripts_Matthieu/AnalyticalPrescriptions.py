# This file contains the analytical functions describing potential, efield and magnetic field for different cases
import math
# Magnetic Fields
def b_field_description1(r,z,theta, Rmajor, Rminor):
    # Radial B-field (linear and max at central surfaces of the torus
    R = math.sqrt((r-Rmajor)**2 + z**2)+0.01+0*theta
    bval = 0.01/R*100  # R in cm
    return [0,bval,0]
def bfieldstrength1(r,z,theta,Rmajor,Rminor):
    # Radial B-field (linear and max at central surfaces of the torus
    R = math.sqrt((r-Rmajor)**2 + z**2)+0.01+0*theta
    bval = 0.01/R*100  # R in cm
    return bval

def b_field_description2(r,z,theta, Rmajor, Rminor):
    # Radial B-field (linear and max at central surfaces of the torus
    R = math.sqrt((r-Rmajor)**2 + z**2)+0.01+0*theta
    bval = 0.01/R*100  # R in cm
    return [0,bval,0]

# Potentials
def potential_description1(r,z,theta,Rmajor,Rminor):
    r = r/100
    Rmajor = Rmajor/100
    z = z/100
    D = math.sqrt((r-Rmajor)**2+z**2)
    potval = math.exp(-D)*500
    return potval

def potential_description2(r,z,theta,Rmajor,Rminor):
    R = (r-Rmajor)/100+0.001
    potval = math.exp(-R)*100
    return potval

# Electric Fields
def e_descr1(r,z,theta, Rmajor):
    r = r/100
    Rmajor = Rmajor/100
    z = z/100
    D = math.sqrt((r - Rmajor) ** 2 + z ** 2)
    Er = -500*math.exp(-D)*1/D*(r-Rmajor)
    Ephi = 0
    Ez = -500*math.exp(-D)*1/D*z
    return [Er,Ephi, Ez]

def e_descr2(r,z,theta, Rmajor):
    D = (r - Rmajor)/100+0.001
    Er = -100*math.exp(-D)
    Ephi = 0
    Ez = 0
    return [Er,Ephi, Ez]