import math
import driftFunctions
import AnalyticalPrescriptions
import Constants
import Utilities
#import pandas as pd
import matplotlib.pyplot as plt
import PlottingFunctions
import AnalysisFunctions

# Constants
drifts_file = open('D:\Matthieu\Documents\\1.Master\Thesis\Data\TestCase2010050GG\Output\ExB_RESULTS','r')
driftpos_file = open('D:\Matthieu\Documents\\1.Master\Thesis\Data\TestCase2010050GG\Output\DRIFT_POS','r')
efield_file = open('D:\Matthieu\Documents\\1.Master\Thesis\Data\TestCase2010050GG\Output\EFIELD','r')
drifts = []
drift_pos = []
efield = []

# Read numeric drift values
for line in drifts_file:
    drifts.append(line)

# Read drift positions
for line in driftpos_file:
    drift_pos.append(line)

# Electric field
for line in efield_file:
    efield.append(line)

# Convert values from fortran to float, filter boundary cells and compute analytical profiles for cell center locations
[drift_pos_num, efield_num, drifts_num, efield_an, bfield_an, drifts_an] = AnalysisFunctions.extractValues(drift_pos,drifts, efield,AnalyticalPrescriptions.e_descr2,AnalyticalPrescriptions.b_field_description2,driftFunctions.ExBdrift)

# Compute vectors and magnitude + errors for efield and drift
[drift_res_mag,drift_an_mag,drift_er_mag,drift_er_vec,drift_er_rel_mag,drift_er_rel_vec] = AnalysisFunctions.extractVectorInfo(drifts_num,drifts_an)
[efield_res_mag,efield_an_mag,efield_er_mag,efield_er_vec,efield_er_rel_mag,efield_er_rel_vec] = AnalysisFunctions.extractVectorInfo(efield_num,efield_an)

# Take slices in different directions to allow for extra analysis
torsurf = drift_pos_num[0][1]
radsurf = drift_pos_num[150][0] # Based on inspection

# Create a dictionary containing the different slices
torSliceDict = {}
keys = ['drifts','efield','ef_mag','ef_an_mag','drift_mag','drift_an_mag','ef_vec_er','ef_mag_er','drift_vec_er','drift_er_mag',
 'efield_er_rel_mag','efield_er_rel_vec','drift_er_rel_mag','drift_er_rel_vec','pos']
torslices = AnalysisFunctions.getAllSlices(drift_pos_num,[drifts_num,efield_num,efield_res_mag,efield_an_mag,drift_res_mag,
                                            drift_an_mag,efield_er_vec,efield_er_mag,drift_er_vec,drift_er_mag,efield_er_rel_mag,
                                            efield_er_rel_vec,drift_er_rel_mag,drift_er_rel_vec],1,torsurf)
for i in range(len(torslices)):
    torSliceDict[keys[i]] = torslices[i]

print(torSliceDict.get('efield_er_rel_mag'))
print(sum(torSliceDict.get('efield_er_rel_mag')))
print(len(torSliceDict.get('efield_er_rel_mag')))
# Plot results

x = [r[0] for r in torSliceDict.get('pos')]
y = [r[1] for r in torSliceDict.get('pos')]
er = [x[0] for x in torSliceDict.get('efield')]
ez = [x[1] for x in torSliceDict.get('efield')]


plot1 = plt.figure(1)
plt.xlabel('Radial Position [cm]')
plt.ylabel('Vertical Position [cm]')
plt.title('R-component of Electic Field')
sc1 = plt.scatter(x, y, s=20, c=[x[0] for x in torSliceDict.get('efield')], cmap='plasma')
plt.colorbar(sc1)


plot2 = plt.figure(2)
plt.xlabel('Radial Position [cm]')
plt.ylabel('Vertical Position [cm]')
plt.title('Z component of Electric Field')
sc2 = plt.scatter(x, y, s=20, c = [x[1] for x in torSliceDict.get('efield')], cmap='plasma')
plt.colorbar(sc2)

plot3 = plt.figure(3)
plt.xlabel('Radial Position [cm]')
plt.ylabel('Vertical Position [cm]')
plt.title('Relative magnitude error of Electric Field')
sc3 = plt.scatter(x, y, s=20, c=torSliceDict.get('efield_er_rel_mag'), cmap='plasma')
plt.colorbar(sc3)

plot4 = plt.figure(4)
plt.title('Potential')
PlottingFunctions.plot2DContour(Constants.ns,Constants.ns,Constants.Rmajor-Constants.Rminor,Constants.Rmajor+Constants.Rminor,-Constants.Rminor,Constants.Rminor,Constants.Rmajor,Constants.Rminor,AnalyticalPrescriptions.potential_description2,'X [cm]', 'Y [cm]','Potential [V]',False)

plot5 = plt.figure(5)
plt.xlabel('Radial Position [cm]')
plt.ylabel('Vertical Position [cm]')
plt.title('Magnitude of ExB Drift (Numerical)')
sc5 = plt.scatter(x, y, s=20, c=torSliceDict.get('drift_mag'), cmap='plasma')
plt.colorbar(sc5)

plot6 = plt.figure(6)
plt.xlabel('Radial Position [cm]')
plt.ylabel('Vertical Position [cm]')
plt.title('Magnitude of ExB Drift (Analytical)')
sc6 = plt.scatter(x, y, s=20, c=torSliceDict.get('drift_an_mag'), cmap='plasma')
plt.colorbar(sc6)

plot7 = plt.figure(7)
plt.xlabel('Radial Position [cm]')
plt.ylabel('Vertical Position [cm]')
plt.title('Relative magnitude error of ExB Drift')
sc7 = plt.scatter(x, y, s=20, c=torSliceDict.get('drift_er_rel_mag'), cmap='plasma')
plt.colorbar(sc7)



plt.show()

