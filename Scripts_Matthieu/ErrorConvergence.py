# Compute an error measure for different mesh resolutions and compare with expected decrease
import AnalysisFunctions
import driftFunctions
import AnalyticalPrescriptions
import matplotlib.pyplot as plt

LSQfiles = []
LSQfiles.append('TestCase2010050LSQ')
LSQfiles.append('TestCase105020LSQ')
LSQfiles.append('TestCase5500500LSQ')
LSQfiles.append('TestCase10100100LSQ')

GGfiles = []
GGfiles.append('TestCase2010050GG')
GGfiles.append('TestCase105020GG')
GGfiles.append('TestCase5500500GG')
GGfiles.append('TestCase10100100GG')

errorsLSQ = []
nbCellsLSQ = []
errorsGG = []
nbCellsGG = []
path = 'D:\Matthieu\Documents\\1.Master\Thesis\Data\\'

for i in range(len(LSQfiles)):
    torSliceDict = AnalysisFunctions.getAllInfo(AnalysisFunctions.readText(path + LSQfiles[i] + '\Output\DRIFT_POS'),
                                                AnalysisFunctions.readText(path+LSQfiles[i]+'\Output\ExB_RESULTS'),
                                                AnalysisFunctions.readText(path + LSQfiles[i] + '\Output\EFIELD'),
                                                AnalyticalPrescriptions.e_descr2, AnalyticalPrescriptions.b_field_description2,
                                                driftFunctions.ExBdrift)
    relerrors = torSliceDict.get('efield_er_rel_mag')
    errorsLSQ.append(sum(relerrors)/len(relerrors))
    nbCellsLSQ.append(len(relerrors))
    print(i)
    print(sum(relerrors))
    print(len(relerrors))
    print(sum(relerrors)/len(relerrors))


for i in range(len(GGfiles)):
    torSliceDict = AnalysisFunctions.getAllInfo(AnalysisFunctions.readText(path + GGfiles[i] + '\Output\DRIFT_POS'),
                                                AnalysisFunctions.readText(path+GGfiles[i]+'\Output\ExB_RESULTS'),
                                                AnalysisFunctions.readText(path + GGfiles[i] + '\Output\EFIELD'),
                                                AnalyticalPrescriptions.e_descr2, AnalyticalPrescriptions.b_field_description2,
                                                driftFunctions.ExBdrift)
    relerrors = torSliceDict.get('efield_er_rel_mag')
    errorsGG.append(sum(relerrors)/len(relerrors))
    nbCellsGG.append(len(relerrors))
    print(i)
    print(sum(relerrors))
    print(len(relerrors))
    print(sum(relerrors)/len(relerrors))

# For both methods, plot the error as a function of the number of cells used
plot1 = plt.figure(1)
sc1 = plt.scatter(nbCellsGG, errorsGG)
ax = plt.gca()
ax.set_yscale('log')
ax.set_xscale('log')
plot2 = plt.figure(2)
sc2 = plt.scatter(nbCellsLSQ,errorsLSQ)
ax = plt.gca()
ax.set_yscale('log')
ax.set_xscale('log')



plt.show()