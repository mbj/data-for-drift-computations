# Functions to extract the information from the fortran output and compare with analytical results

import Constants
import math
import Utilities

def extractValues(positions, drifts, efield, edescr, bdescr, ExB):
    pos = []
    enum = []
    dnum = []
    ea = []
    ba = []
    da = []
    for i in range(len(drifts)):
        # Convert to float values to allow computations and interpretation
        dn = [float(x) for x in drifts[i].split()]
        dpn = [float(x) for x in positions[i].split()]
        en = [float(x) for x in efield[i].split()]

        # filter our boundary cells => These have zero field and drift and are not useful for interpretation!
        if dn != [0,0,0]:
            dnum.append(dn)
            pos.append(dpn)
            enum.append(en)
            # Compute analytical efield and drifts at these positions
            efa = edescr(dpn[0], dpn[2], dpn[1], Constants.Rmajor)
            bfa = bdescr(dpn[0], dpn[2], dpn[1], Constants.Rmajor, Constants.Rminor)
            ea.append(efa)
            ba.append(ba)
            da.append(ExB(efa, bfa).tolist())
    return [pos, enum, dnum, ea , ba, da]

def extractVectorInfo(numeric,analytical):
    res_mag = []
    an_mag = []
    er_mag = []
    er_vec = []
    er_rel_vec = []
    er_rel_mag = []
    for i in range(len(numeric)):
        res_mag.append(math.sqrt(sum([x**2 for x in numeric[i]])))
        an_mag.append(math.sqrt(sum([x**2 for x in analytical[i]])))
        er_mag.append(math.sqrt((res_mag[i]-an_mag[i])**2))
        er_vec.append(Utilities.listdif(numeric[i],analytical[i]))
        er_rel_vec.append([math.sqrt(x**2)/an_mag[i] for x in er_vec[i]])
        er_rel_mag.append(er_mag[i]/an_mag[i])

    return [res_mag,an_mag,er_mag,er_vec,er_rel_mag,er_rel_vec]

def slice(positions,info,direction, coordinate):
    # Get a slice of the full range of information along a certain direction
    # positions are the coordinates corresponding to the info values
    # direction: between 0 and 2: along which direction to take a slice!
    # coordinate specifies which surface to keep as slice
    pos_slice = []
    info_slice = []
    index1 = direction-1 %3
    index2 = direction+1 %3
    for i in range(len(positions)):
        if positions[i][direction] == coordinate:
            pos_slice.append([positions[i][index1],positions[i][index2]])
            info_slice.append(info[i])

    return [pos_slice, info_slice]

def getAllSlices(positions,allInfo,direction,coordinate):
    pos_slice = []
    result = []
    for i in range(len(allInfo)):
        [pos_slice, info_slice] = slice(positions,allInfo[i],direction,coordinate)
        result.append(info_slice)
    result.append(pos_slice)
    return result

def getAllInfo(positions,drifts,efield,edescr,bdescr,ExB):
    [pos, enum, dnum, ea, ba, da] = extractValues(positions, drifts, efield, edescr, bdescr, ExB)

    [dres_mag, dan_mag, der_mag, der_vec, der_rel_mag, der_rel_vec] =  extractVectorInfo(dnum,da)
    [eres_mag, ean_mag, eer_mag, eer_vec, eer_rel_mag, eer_rel_vec] = extractVectorInfo(enum, ea)

    torsurf = pos[0][1]

    torSliceDict = {}
    keys = ['drifts', 'efield', 'ef_mag', 'ef_an_mag', 'drift_mag', 'drift_an_mag', 'ef_vec_er', 'ef_mag_er',
            'drift_vec_er', 'drift_er_mag',
            'efield_er_rel_mag', 'efield_er_rel_vec', 'drift_er_rel_mag', 'drift_er_rel_vec', 'pos']
    #torslices = getAllSlices(pos,[dres_mag, dan_mag, der_mag, der_vec, der_rel_mag, der_rel_vec,eres_mag, ean_mag, eer_mag, eer_vec, eer_rel_mag, eer_rel_vec],1,torsurf)

    values = [dnum,enum,eres_mag,ean_mag,dres_mag,dan_mag,eer_vec,eer_mag,der_vec,der_mag,eer_rel_mag,eer_rel_vec,der_rel_mag,der_rel_vec,pos]
    for i in range(len(values)):
        torSliceDict[keys[i]] = values[i]

    return torSliceDict


def readText(filepath):
    file = open(filepath,'r')
    result = []
    for line in file:
        result.append(line)
    return result





