# This script calls python functions allowing to create a mesh for some simple computations

# Import the required functions from "MeshFunctions"
from MeshFunctions import *

[tor,rad,hor] = create_toroidal_vertices(6,5,10,6,500,100)
writeGEOMETRY3DDATA(tor,rad,hor)

imposeAnalytic(tor,rad,hor,b_field_description,"BFIELDSTRENGTH")

