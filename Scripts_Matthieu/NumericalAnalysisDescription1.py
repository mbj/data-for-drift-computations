import math
import driftFunctions
import AnalyticalPrescriptions
import Constants
import Utilities
#import pandas as pd
import matplotlib.pyplot as plt
import PlottingFunctions
import AnalysisFunctions

# Constants
path = 'D:\Matthieu\Documents\\1.Master\Thesis\Data\Description1\\'

GGfiles = []
GGfiles.append('Case50GG')
GGfiles.append('Case100GG')
GGfiles.append('Case200GG')
GGfiles.append('Case400GG')
GGfiles.append('Case1000GG')
GGfiles.append('Case10000GG')


errorsLSQ = []
nbCellsLSQ = []
errorsGG = []
nbCellsGG = []

for i in range(len(GGfiles)):
    torSliceDict = AnalysisFunctions.getAllInfo(AnalysisFunctions.readText(path + GGfiles[i] + '\Output\DRIFT_POS'),
                                                AnalysisFunctions.readText(path+GGfiles[i]+'\Output\ExB_RESULTS'),
                                                AnalysisFunctions.readText(path + GGfiles[i] + '\Output\EFIELD'),
                                                AnalyticalPrescriptions.e_descr2, AnalyticalPrescriptions.b_field_description2,
                                                driftFunctions.ExBdrift)
    relerrors = torSliceDict.get('efield_er_rel_mag')
    errorsGG.append(sum(relerrors)/len(relerrors))
    nbCellsGG.append(len(relerrors))
    print(i)
    print(sum(relerrors))
    print(len(relerrors))
    print(sum(relerrors)/len(relerrors))


# For both methods, plot the error as a function of the number of cells used
plot1 = plt.figure(1)
sc1 = plt.scatter(nbCellsGG, errorsGG)
ax = plt.gca()
ax.set_yscale('log')
ax.set_xscale('log')
# plot2 = plt.figure(2)
# sc2 = plt.scatter(nbCellsLSQ,errorsLSQ)
# ax = plt.gca()
# ax.set_yscale('log')
# ax.set_xscale('log')



plt.show()