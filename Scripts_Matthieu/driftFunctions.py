import numpy as np
import math
def ExBdrift(E,B):
    E = np.array(E)
 #   print('E', E)
    B = np.array(B)
#    print('B',B)
    normB = np.linalg.norm(B)
    return np.cross(E,B)*1/normB**2

def gradBdrift(B,gradB):
    Tq = 150 # keV => no e for electroncharge (should be 000)
    return 2*Tq * np.cross(B,gradB)/np.linalg.norm(B)**3

def cyltocart(V,p):
    alpha = p[1]
    C = np.array([0.0,0.0,0.0])
    C[0] = -math.sin(alpha)*V[1] + math.cos(alpha)*V[0]
    C[1] = math.cos(alpha)*V[1] + math.sin(alpha)*V[0]
    C[2] = V[2]
    return C

def cylvec(V,p):
    C = np.array([0.0,0.0,0.0])
    dif = np.array([0.0,0.0,0.0])
    pcart = np.array([0.0,0.0,0.0])
    V = np.array(V)
    p = np.array(p)/100
    V[0] += p[0]
    V[1] += p[1]
    V[2] += p[2]
    pcart[0] = p[0]*math.cos(p[1])
    pcart[1] = p[1]*math.sin(p[1])
    pcart[2] = p[2]
    C[0]= V[0]*math.cos(V[1])
    C[1] = V[0]*math.sin(V[1])
    C[2] = V[2]
    dif = C-pcart
    return dif
def carttocyl(V):
    C = np.array([0, 0, 0])
    C[0] = math.sqrt(V[0]**2+V[1]**2)
    C[1] = math.atan2(V[1],V[0])
    C[2] = V[2]
    return C