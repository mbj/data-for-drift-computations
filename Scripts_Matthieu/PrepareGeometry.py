# This file defines all the quantities and creates the files required to run a test case for the computation of
# drift velocities in the EMC3 simulation code
from MeshFunctions import *
import os
import shutil
# The folder where the different cases should be stored is
folder = "D:\Matthieu\Documents\\1.Master\Thesis\Data"
# A specific folder for the case should be defined
casefolder = "TestCase"
# create the folder
path = folder+"\\"+casefolder
# Check if the folder should be removed => Control to not loose simulations
if os.path.isdir(path):
    Flag = int(input("Do you want to remove"+path+" Answer should be 1/0"))
    if Flag:
        shutil.rmtree(path)
    else:
        path = folder+"\\"+input("Give a new folder name")
os.mkdir(path)
path = path+"\\Inputs"
os.mkdir(path)
# Choose the parameters for the case to test
# Geometry
# Number of zones considered
nz = 1
# Number of toroidal, radial, poloidal surfaces
nt = 20
nr = 10*10
np = 10*5
# toroidal range (degrees)
trange = 36
# Major and minor radius (considering a torus) (in cm!)
Rmajor = 500
Rminor = 100
# Create the file with the general info
gi_path = path+"\\input.geo"
CreateGeneralInfo(gi_path, nz,[nr],[np],[nt])


# Create the GEOMETRY_3D_DATA file
[tor,rad,hor] = create_toroidal_vertices(nt,nr,np,trange,Rmajor,Rminor)
print(tor)
writeGEOMETRY3DDATA(tor,rad,hor,nt,nr,np,path)

# Create the magnetic field data
# Create an analytical description
def b_field_description(r,z,theta, Rmajor, Rminor):
    # Radial B-field (linear and max at central surfaces of the torus
    R = math.sqrt((r-Rmajor)**2 + z**2)+0.01+0*theta
    bval = 0.01/R*100  # R in cm
    return bval
bfieldpath = path+"\\BFIELD_STRENGTH"
imposeAnalytic(tor,rad,hor,b_field_description,Rmajor, Rminor, bfieldpath)

# Create the potential data
# At the moment we use the same grid for magnetic and plasma cells
def potential_description(r,z,theta,Rmajor,Rminor):
    R = (r-Rmajor)
    potval = math.exp(-R/100)*100
    return potval
potentialpath = path+"\\POTENTIAL"
imposeAnalytic(tor,rad,hor,potential_description,Rmajor, Rminor,potentialpath)
