* input file for EMC3 code
*** 1. fein grid mesh representing magnetic coordinates
1
100   100   5   
*** 2. Non default, Non Transparant, Plate surface
*** 2.1 Non default
* radial
0
* poloidal
2
0  0  1
0  98  0  3
99  0  1
0  98  0  3
* toroidal
2
0  0  3
0  98  0  98
4  0  3
0  98  0  98
*** 2.2 Non transparant
* radial
2
3  0  1
0  98  0  3
98  0  -1
0  98  0  3
* poloidal
0
* toroidal
0
*** 2.3 Plate surface
* radial
0
* poloidal
0
* toroidal
2
3  0  1  
97  98  0  98
3  0  -1  
97  98  0  98
*** 3. Physical cell
10  1
* check cell ?
F