* input file for EMC3 code
*** 1. fein grid mesh representing magnetic coordinates
1
100   50   20   
*** 2. Non default, Non Transparant, Plate surface
*** 2.1 Non default
* radial
0
* poloidal
2
0  0  1
0  98  0  18
49  0  1
0  98  0  18
* toroidal
2
0  0  3
0  98  0  48
19  0  3
0  98  0  48
*** 2.2 Non transparant
* radial
2
3  0  1
0  48  0  18
98  0  -1
0  48  0  18
* poloidal
0
* toroidal
0
*** 2.3 Plate surface
* radial
0
* poloidal
0
* toroidal
2
18  0  1  
97  98  0  48
18  0  -1  
97  98  0  48
*** 3. Physical cell
10  1
* check cell ?
F